package com.reacttoc;

import android.telecom.Call;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.HashMap;

public class CFunctions extends ReactContextBaseJavaModule {
    static {
        System.loadLibrary("hello_world_jni"); //this loads the library when the class is loaded
    }

    CFunctions(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "CFunctions";
    }

    @ReactMethod
    public void helloWorld(Promise promise) {
        try {
            String hello = helloWorldJNI();
            promise.resolve(hello);
        } catch (Exception e) {
            promise.reject("ERR", e);
        }
    }

    public native String helloWorldJNI();

}