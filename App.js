/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import CallNative from './components/CallNative';

const App = () => {
  return <CallNative />;
};

export default App;
