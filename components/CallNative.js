import React, {Fragment} from 'react';
import {NativeModules, Button} from 'react-native';

const {CFunctions} = NativeModules;

const CallNative = () => {
  const onPress = async () => {
    try {
      await CFunctions.helloWorld().then(data => {
        console.log(data);
      });
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <Fragment>
      <Button title="Call Function" color="#841584" onPress={onPress} />
    </Fragment>
  );
};

export default CallNative;
