LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

MY_C_LIST :=  $(wildcard $(LOCAL_PATH)/*.c)
MY_C_LIST += $(wildcard $(LOCAL_PATH)/iperf/*.c)


LOCAL_MODULE := hello_world_jni
LOCAL_SRC_FILES := $(MY_C_LIST:$(LOCAL_PATH)/%=%)


include $(BUILD_SHARED_LIBRARY)